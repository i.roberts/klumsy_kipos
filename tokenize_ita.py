import spacy
import sys



if __name__ == '__main__':
    nlp = spacy.load('it_core_news_sm', disable=['parser', 'ner'])
    nlp.add_pipe('sentencizer')
    text = sys.argv[1]
    doc = nlp(text)
    with open('temp.tsv', 'w') as temp:
        for (i,sent) in enumerate(doc.sents):
            for (j,tok) in enumerate(sent):
                temp.write(str(tok) + '\n')
            temp.write('\n')